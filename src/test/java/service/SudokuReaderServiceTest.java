package service;


import org.junit.jupiter.api.Test;

import java.io.FileNotFoundException;
import java.io.IOException;

import static constants.ErrorMessagesEnum.INVALID_LINE_LENGTH;
import static constants.ErrorMessagesEnum.INVALID_VALUE;
import static org.junit.jupiter.api.Assertions.*;


class SudokuReaderServiceTest {

    @Test
    public void validFile() throws IOException {
        SudokuReaderService service = new SudokuReaderService();
        int[][] matrix = service.readMatrixFromFile("src/test/resources/test-files/simple-sudoku.txt");
        assertArrayEquals(matrix[0], simpleSudoku()[0]);
    }

    @Test
    public void testWhenFileNotFound() {
        assertThrows(FileNotFoundException.class, () -> {
            SudokuReaderService service = new SudokuReaderService();
            service.readMatrixFromFile("wrongPath/invalid-simple-valid-sudoku.txt");
        });
    }

    @Test
    public void testWhenFileIsEmpty() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            SudokuReaderService service = new SudokuReaderService();
            service.readMatrixFromFile("src/test/resources/test-files/empty-sudoku.txt");
        });
        assertEquals(exception.getMessage(), INVALID_LINE_LENGTH.getErrorMessage());
    }

    @Test
    public void testWhenFileHasInvalidCharacter() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            SudokuReaderService service = new SudokuReaderService();
            service.readMatrixFromFile("src/test/resources/test-files/invalid-char-sudoku.txt");
        });
        assertEquals(exception.getMessage(), INVALID_VALUE.getErrorMessage());
    }

    @Test
    public void testWhenFileHasInvalidNumberRange() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            SudokuReaderService service = new SudokuReaderService();
            service.readMatrixFromFile("src/test/resources/test-files/invalid-value-sudoku.txt");
        });
        assertEquals(exception.getMessage(), INVALID_VALUE.getErrorMessage());
    }

    @Test
    public void testWhenFileHasInvalidRowLength() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            SudokuReaderService service = new SudokuReaderService();
            service.readMatrixFromFile("src/test/resources/test-files/invalid-row-length-sudoku.txt");
        });
        assertEquals(exception.getMessage(), INVALID_LINE_LENGTH.getErrorMessage());
    }

    private int[][] simpleSudoku() {
        int[][] matrix = new int[9][9];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                matrix[i][j] = j + 1;
            }
        }
        return matrix;
    }
}