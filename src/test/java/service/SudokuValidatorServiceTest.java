package service;

import constants.SudokuResultEnum;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static constants.ErrorMessagesEnum.INVALID_MATRIX_VALUE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class SudokuValidatorServiceTest {


    @Test
    public void testValidSudoku() throws IOException {
        SudokuReaderService service = new SudokuReaderService();
        int[][] matrix = service.readMatrixFromFile("src/test/resources/test-files/valid-sudoku.txt");
        assertEquals(SudokuResultEnum.VALID.getCode(), SudokuValidatorService.isValidSudoku(matrix));
    }

    @Test
    public void testWhenFileHasInvalidNumberRange() throws IOException {
        SudokuReaderService service = new SudokuReaderService();
        int[][] matrix = service.readMatrixFromFile("src/test/resources/test-files/invalid-sudoku.txt");
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            SudokuValidatorService.isValidSudoku(matrix);
        });
        assertEquals(exception.getMessage(), INVALID_MATRIX_VALUE.getErrorMessage());
    }
}