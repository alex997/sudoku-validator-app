package constants;

public enum ErrorMessagesEnum {
    INVALID_LINE_LENGTH("Does not valid length of line from file."),
    INVALID_VALUE("Invalid value used in file. Value should be specified from 1 to 9 inclusive."),
    EMPTY_FILE_PATH("File path is not specified."),
    INVALID_MATRIX_VALUE("Invalid value used in matrix. Sudoku is invalid!");

    private final String errorMessage;

    ErrorMessagesEnum(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
