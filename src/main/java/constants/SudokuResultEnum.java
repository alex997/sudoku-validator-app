package constants;

public enum SudokuResultEnum {
	VALID(0),INVALID(-1);

	private final int code;

	SudokuResultEnum(int code) {
		this.code = code;
	}
	public int getCode() {
		return this.code;
	}
}
