package config;

public class Logger {

    public static void printResult(int code) {
        System.out.println("Result : " + code);
    }

    public static void printError(String message) {
        System.err.println(message);
    }
}
