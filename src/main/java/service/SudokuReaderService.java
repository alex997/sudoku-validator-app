package service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import static constants.ErrorMessagesEnum.INVALID_LINE_LENGTH;
import static constants.ErrorMessagesEnum.INVALID_VALUE;


public class SudokuReaderService {
    private static final String COMMAS_SEPARATOR = ",";

    public int[][] readMatrixFromFile(String filePath) throws IOException {
        int[][] matrix = new int[9][9];
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String lineContent;
            int rowNumber = 0;
            do {
                lineContent = br.readLine();
                if (lineContent != null) {
                    String[] values = lineContent.split(COMMAS_SEPARATOR);
                    fillMatrixLine(matrix, values, rowNumber++);
                }
            } while (lineContent != null);
            if (rowNumber == 0)
                throw new IllegalArgumentException(INVALID_LINE_LENGTH.getErrorMessage());
        }
        return matrix;
    }

    private void fillMatrixLine(int[][] matrix, String[] values, int rowNumber) {
        if (values == null || values.length != 9)
            throw new IllegalArgumentException(INVALID_LINE_LENGTH.getErrorMessage());
        for (int j = 0; j < values.length; j++) {
            try {
                matrix[rowNumber][j] = Integer.parseInt(values[j]);
                if (matrix[rowNumber][j] < 1 || matrix[rowNumber][j] > 9)
                    throw new IllegalArgumentException(INVALID_VALUE.getErrorMessage());
            } catch (NumberFormatException ex) {
                throw new IllegalArgumentException(INVALID_VALUE.getErrorMessage());
            }
        }
    }

}


