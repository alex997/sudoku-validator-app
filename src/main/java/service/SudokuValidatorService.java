package service;

import constants.SudokuResultEnum;

import java.util.HashSet;
import java.util.Set;

import static constants.ErrorMessagesEnum.INVALID_MATRIX_VALUE;

public class SudokuValidatorService {

    public static int isValidSudoku(int[][] matrix) throws IllegalArgumentException {
        validateRows(matrix);
        validateColumns(matrix);
        validateSubSquare(matrix);
        return SudokuResultEnum.VALID.getCode();
    }

    private static void validateRows(int[][] matrix) throws IllegalArgumentException {
        Set<Integer> rowsSet = new HashSet<>();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (!rowsSet.add(matrix[i][j])) {
                    throw new IllegalArgumentException(INVALID_MATRIX_VALUE.getErrorMessage());
                }
            }
            rowsSet.clear();
        }
    }

    private static void validateColumns(int[][] matrix) throws IllegalArgumentException {
        Set<Integer> columnsSet = new HashSet<>();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (!columnsSet.add(matrix[j][i])) {
                    throw new IllegalArgumentException(INVALID_MATRIX_VALUE.getErrorMessage());
                }
            }
            columnsSet.clear();
        }
    }

    private static void validateSubSquare(int[][] matrix) throws IllegalArgumentException {
        int subMatrixLength = (int) Math.sqrt(matrix.length);
        for (int i = 0; i < subMatrixLength; i++) {
            for (int j = 0; j < subMatrixLength; j++) {
                validateSubSquareDuplicates(matrix, subMatrixLength * i, subMatrixLength * (i + 1),
                        subMatrixLength * j, subMatrixLength * (j + 1));
            }
        }
    }

    private static void validateSubSquareDuplicates(int[][] matrix, int startRow, int endRow,
                                                    int startColumn, int endColumn) throws IllegalArgumentException {
        Set<Integer> set = new HashSet<>();
        for (int i = startRow; i < endRow; i++) {
            for (int j = startColumn; j < endColumn; j++) {
                if (!set.add(matrix[i][j])) {
                    throw new IllegalArgumentException(INVALID_MATRIX_VALUE.getErrorMessage());
                }
            }
            set.clear();
        }
    }
}
