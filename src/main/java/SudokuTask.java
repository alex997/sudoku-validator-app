import config.Logger;
import constants.SudokuResultEnum;
import service.SudokuReaderService;
import service.SudokuValidatorService;

import java.io.IOException;

import static constants.ErrorMessagesEnum.EMPTY_FILE_PATH;

public class SudokuTask {

    public static void main(String[] args) {
        try {
            if (args.length == 0)
                throw new IllegalArgumentException(EMPTY_FILE_PATH.getErrorMessage());
            SudokuReaderService readerService = new SudokuReaderService();
            Logger.printResult(
                    SudokuValidatorService.isValidSudoku(readerService.readMatrixFromFile(args[0])));
        } catch (IOException | IllegalArgumentException ex) {
            Logger.printResult(SudokuResultEnum.INVALID.getCode());
            Logger.printError(ex.getMessage());
        }
    }
}


